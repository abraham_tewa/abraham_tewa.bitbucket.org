'use strict';
var ZOOM_DELTA_LEVEL  = 10;
var INITIAL_FONT_SIZE = 20;
var MAIN_ELEMENT;
var MAX_ZOOM_LEVEL = 90;

var mainElements;

var svgNS = "http://www.w3.org/2000/svg";

var empireList = { "Faërie"       : ["Ansemer", "Cibella", "Lagrance", "Outrevent"]
                 , "Ibélène"      : ["Bellifère", "Erebor", "Sombreciel", "Valkyrion"]
                 , "TerresDuNord" : ["TerresDuNord"]

};

function calculate_distance(deltaX, deltaY) {
    return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
}

function onload() {

    MAIN_ELEMENT = document.querySelector('main');

    mainElements = { background : document.querySelector('img#background')
                   , main       : MAIN_ELEMENT
                   , svg        : document.querySelector('svg')};

    Empire.create('Faërie'      , empireList['Faërie']);
    Empire.create('Ibélène'     , empireList['Ibélène']);
    Empire.create('TerresDuNord', empireList['TerresDuNord']);

    ImagesHandler.initialize();
    MeasureHandler.initialize();
    MouseHandler.initialize();
    DragHandler.initialize();
    ZoomHandler.initialize();
}

var ImagesHandler = (function() {

    function initialize() {
        initializeBase64Images();
        initializeSVG();
    }

    function initializeBase64Images() {

        var assetName
          , dataType
          , img;

        for(assetName in assets) {

            if (assetName === '_')
                continue;

            if (assetName.substr(assetName.length - 4) === '.jpg')
                dataType = 'data:image/jpeg;base64,';
            else
                dataType = 'data:image/png;base64,';

            img = document.querySelector('img[data-src="' + assetName + '"]');
            img.setAttribute('src', dataType + assets[assetName]);
        }
    }

    function initializeSVG() {

        var d
          , duchy
          , duchies;

        mainElements.svg.classList.add('map');

        duchies = mainElements.svg.querySelectorAll('path');

        for(d=0, duchy=duchies[d]; d < duchies.length; d++, duchy=duchies[d]) {
            duchy.setAttribute('data-duchy-code', duchy.getAttribute('id'));
            duchy.setAttribute('fill-opacity', '0');
        }
    }

    return {initialize: initialize};
})();

var Duchy          = (function() {

    var Duchies = {};

    var DuchiesList = [ 'Ansemer'
                      , 'Bellifère'
                      , 'Cibella'
                      , 'Erebor'
                      , 'Lagrance'
                      , 'Outrevent'
                      , 'Sombreciel'
                      , 'TerresDuNord'
                      , 'Valkyrion'];

    function Duchy(name) {
        this.name  = name;
        this.selected = false;

        this.element = document.querySelector('#' + this.name + 'Duchy');
        this.path  = document.querySelector('path#' + this.name);
        this.label = document.querySelector('#' + this.name + 'Duchy > .duchyLabel');
    }

    Duchy.prototype.declareEmpire = function declareEmpire(empire) {
        this.empire = empire;
    };

    Duchy.prototype.isSelected = function isSelected() {
        return this.selected;
    };

    Duchy.prototype.toggleSelect = function toggleSelect(force) {
        var classToggle;

        // Toggle class
        classToggle = this.path.classList.toggle('selected', force);
        this.element.classList.toggle('selected', classToggle);

        this.selected = classToggle;

        this.empire.refresh();
    };

    Duchy.forEach = function(callback, thisArg) {
        DuchiesList.forEach(callback, thisArg);
    };

    Duchy.get = function(duchy) {

        var name;

        name = Duchy.getName(duchy);

        if (Duchies[name])
            return Duchies[name];

        Duchies[name] = new Duchy(name);
        return Duchy.get(name);
    };

    /**
     *
     * @param {string|Element} duchy
     * @returns {Element}
     */
    Duchy.getElement = function getElement(duchy) {
        var /** @type {string} */ duchyName;
    
        duchyName = Duchy.getName(duchy);
        return Duchy.get(duchyName).element;
    };
    
    Duchy.getLabel = function getLabel(duchy) {
        var /** @type {string} */ duchyName;
    
        duchyName = Duchy.getName(duchy);
        return Duchy.get(duchyName).label;
    };
    
    /**
     *
     * @param {string|Element} element
     * @returns {string}
     */
    Duchy.getName = function getName(element) {
        if (typeof element === 'string')
            return element;
    
        return element.getAttribute('data-duchy-code');
    };
    
    /**
     *
     * @param {string|Element} duchy
     * @returns {Element}
     */
    Duchy.getPath = function getPath(duchy) {
        var duchyName;
        duchyName = Duchy.getName(duchy);
        return Duchy.get(duchyName).path;
    };

    return Duchy;
})();

var DragHandler    = (function() {

    var DRAG_START_POSITION
      , INITIAL_POSITION
      , INITIAL_WINDOW
      , LAST_POSITION
      , STATUS
      , status;

    INITIAL_POSITION = {x: 0, y: 0};
    INITIAL_WINDOW   = {innerHeight: 0, innerWidth: 0};

    STATUS = { NONE    : 'none'
             , STARTED : 'started'};

    function adjustMap() {
        var marginLeft
          , marginTop;

        marginTop  = MAIN_ELEMENT.style.marginTop;
        marginLeft = MAIN_ELEMENT.style.marginLeft;

        changeMargin(marginLeft, marginTop);
    }

    function changeMargin(marginLeft, marginTop) {

        if (marginTop > 0 || ZoomHandler.mapHeight <= INITIAL_WINDOW.innerHeight)
            marginTop = 0;
        else if (INITIAL_WINDOW.innerHeight - marginTop > ZoomHandler.mapHeight && ZoomHandler.mapHeight > INITIAL_WINDOW.innerHeight)
            marginTop = INITIAL_WINDOW.innerHeight - ZoomHandler.mapHeight;

        if (marginLeft > 0 || ZoomHandler.mapWidth < window.innerWidth)
            marginLeft = 0;
        else if (window.innerWidth - marginLeft > ZoomHandler.mapWidth && ZoomHandler.mapWidth > window.innerWidth)
            marginLeft = window.innerWidth - ZoomHandler.mapWidth;

        LAST_POSITION = {x: marginLeft, y: marginTop};

        DragHandler.position = LAST_POSITION;

        mainElements.main.style.marginTop  = marginTop  + 'px';
        mainElements.main.style.marginLeft = marginLeft + 'px';
    }

    function refresh() {

        var margins;

        if (LAST_POSITION)
            margins = {x: LAST_POSITION.x, y: LAST_POSITION.y};
        else
            margins = {x:0, y:0};

        changeMargin(margins.x, margins.y);
    }

    function clearDrag() {
        DRAG_START_POSITION = undefined;
        updateInitialValues();

        mainElements.main.classList.remove('moving');
        document.querySelector('svg.map').classList.remove('moving');
        document.querySelector('img.map').classList.remove('moving');
        document.querySelector('#empires').classList.remove('moving');
    }

    function initialize() {
    }

    function move(positionX, positionY) {
        var marginLeft
          , marginTop;

        if (status !== STATUS.STARTED)
            return;

        marginTop  = positionY - (DRAG_START_POSITION ? DRAG_START_POSITION.y : 0) + INITIAL_POSITION.y;
        marginLeft = positionX - (DRAG_START_POSITION ? DRAG_START_POSITION.x : 0) + INITIAL_POSITION.x;

        changeMargin(marginLeft, marginTop);
    }

    function start(positionX, positionY) {

        status = STATUS.STARTED;

        DRAG_START_POSITION = { x : positionX
                              , y : positionY };

        updateInitialValues();

        mainElements.main.classList.add('moving');
        mainElements.main.style.cursor = 'move';

        document.querySelector('svg.map').classList.add('moving');
        document.querySelector('img.map').classList.add('moving');
        document.querySelector('#empires').classList.add('moving');
    }

    function stop() {
        status = STATUS.NONE;

        clearDrag();
        INITIAL_POSITION = LAST_POSITION ? LAST_POSITION : {x:0, y:0};
    }

    function updateInitialValues() {
        INITIAL_WINDOW = { innerHeight: window.innerHeight
                         , innerWidth : window.innerWidth};
    }

    return { adjustMap  : adjustMap
           , initialize : initialize
           , move       : move
           , position   : {x: 0, y: 0}
           , refresh    : refresh
           , start      : start
           , stop       : stop}

})();

var Empire         = (function() {

    var empires = {};

    function Empire(name, duchiesName) {
        this.name = name;
        this.duchies = duchiesName.map(function(duchyName) {
            var duchy;
            duchy = Duchy.get(duchyName);
            duchy.declareEmpire(this);
            return duchy;
        }, this);

        this.dom = document.querySelector('div.empire#' + this.name + 'Empire');
        this.capitalLabel = this.dom.querySelector('span.empireCapitalLabel');
    }

    Empire.prototype.refresh = function refresh() {

        var isSelected
          , selectedDuchies;

        selectedDuchies = this.duchies.filter(function(duchy) {
            return duchy.isSelected();
        }, this);

        isSelected = selectedDuchies.length === this.duchies.length;

        this.dom.classList.toggle('selected', isSelected);
    };

    Empire.create = function create(name, duchiesName) {
        return new Empire(name, duchiesName);
    };

    Empire.get = function get(name) {
        return empires[name];
    };

    return Empire;
})();

var MouseHandler   = (function() {

    var currentStatus
      , firstClickEvent
      , measureStatus
      , startMove;

    function addEventListener(eventType, listener) {
        window.addEventListener(eventType, listener);
        mainElements.background.addEventListener(eventType, listener);
        mainElements.main.addEventListener(eventType, listener);

        Duchy.forEach(function(duchyName) {
            var duchy
              , path;

            path  = Duchy.getPath(duchyName);
            duchy = Duchy.getElement(duchyName);

            path.addEventListener(eventType , listener, true);
            duchy.addEventListener(eventType, listener, true);
        });
    }

    function initialize() {
        window.addEventListener('wheel', onWheel);
        document.querySelector('svg.map').addEventListener('mouseoout', onMouseOutWindow);

        addEventListener('mousemove', onMove);
        addEventListener('mousedown', onMouseDown);
    }

    function onMouseDown(event) {

        if (!isValidClick(event))
            return;

        event.stopImmediatePropagation();
        event.preventDefault();

        addEventListener('mouseup', onMouseUp);

        if (event.button === 2 || MeasureHandler.isStarted()) {
            return;
        }

        currentStatus = 'mouseDown';

        startMove = {x : event.clientX, y: event.clientY};
    }

    function onMouseOutWindow(event) {
        if (currentStatus === 'moving')
            stopMoving();
    }

    function onMouseUp(event) {
        event.stopImmediatePropagation();
        event.preventDefault();

        if (!isValidClick(event))
            return;

        if (event.button === 2 || MeasureHandler.isStarted()) {
            removeEventListener('mouseup', onMouseUp);
            MeasureHandler.point(event.clientX, event.clientY);
            return;
        }

        if (MeasureHandler.isStarted())
            return;

        // Stopping : moving
        if (currentStatus === 'moving') {
            stopMoving();
            return;
        }

        if (currentStatus === 'mouseDown')
            stopMoving();

        // Stopping : measurement
        if (measureStatus === 'started') {
            currentStatus   = undefined;
            firstClickEvent = undefined;
            return;
        }

        // Double click
        if (firstClickEvent) {
            firstClickEvent = undefined;
            currentStatus   = undefined;
            return;
        }

        // Simple click
        firstClickEvent = event;

        // If the event has been cancelled (because of double click), then we do nothing
        if (firstClickEvent !== event)
            return;

        firstClickEvent = undefined;
        Duchy.get(event.target).toggleSelect();
    }

    function onMove(event) {

        event.stopImmediatePropagation();
        event.preventDefault();

        MeasureHandler.move(event.clientX, event.clientY);

        switch(currentStatus) {
            case 'mouseDown':
                // We start the moving mode only if the user move enough
                if (calculate_distance( Math.abs(startMove.x - event.clientX)
                                      , Math.abs(startMove.y - event.clientY)) < 20) {
                    return;
                }

                currentStatus = 'moving';
                DragHandler.start(startMove.x, startMove.y);
                onMove(event);
                break;

            case 'moving':
                DragHandler.move(event.clientX, event.clientY);
                break;
        }
    }

    function onWheel(event) {
        var zoomMode;

        event.preventDefault();

        zoomMode = event.deltaY > 0 ? 'out' : 'in';

        ZoomHandler.zoom(zoomMode, event.clientX, event.clientY);
    }

    function removeEventListener(eventType, listener) {
        window.removeEventListener(eventType, listener);
        mainElements.background.removeEventListener(eventType, listener);
        mainElements.main.removeEventListener(eventType, listener);

        Duchy.forEach(function(duchyName) {
            var duchy
              , path;

            path  = Duchy.getPath(duchyName);
            duchy = Duchy.getElement(duchyName);

            path.removeEventListener(eventType , listener, true);
            duchy.removeEventListener(eventType, listener, true);
        });
    }

    function stopMoving() {
        startMove = undefined;
        removeEventListener('mouseup', onMouseUp);
        currentStatus = undefined;
        DragHandler.stop();
    }

    function isValidClick(event) {
        if (['HTML', 'SVG', 'A'].indexOf(event.target.tagName) >= 0)
            return false;

        return event.target.getAttribute('id') !== 'Frame';
    }

    return {initialize : initialize};

})();

var MeasureHandler = (function() {

    var lastPosition
      , line
      , initialPoint
      , pixelRatio
      , STATUS
      , status
      , svg
      , text
      , textBackground;

    STATUS = { NONE     : 'none'
             , STARTED  : 'started'
             , FINISHED : 'finished'};

    status = STATUS.NONE;

    pixelRatio = 2;

    function clear() {
        status = STATUS.NONE;

        svg.classList.remove('measureEnable');

        line.setAttribute('x1', '0');
        line.setAttribute('y1', '0');
        line.setAttribute('x2', '0');
        line.setAttribute('y2', '0');
    }

    function calcSVGPosition(coord) {
        return { x : parseInt(coord.x * ZoomHandler.pixelRatio.x)
               , y : parseInt(coord.y * ZoomHandler.pixelRatio.y) };
    }

    function calcTextPosition(coord) {
        var /** @type {number} */ middleX
          , /** @type {number} */ middleY;

        middleX = parseInt((initialPoint.x + coord.x)/2 - DragHandler.position.x);
        middleY = parseInt((initialPoint.y + coord.y)/2 - DragHandler.position.y);

        return calcSVGPosition({x: middleX, y: middleY});
    }

    function initialize() {
        var g;
        svg = document.querySelector('svg');

        // g
        g = document.createElementNS(svgNS, "g");
        g.classList.add('measure');

        // text
        text = document.createElementNS(svgNS, "text");
        text.setAttribute('x', '0');
        text.setAttribute('y', '0');
        text.classList.add('measure');

        // line
        line = document.createElementNS(svgNS, 'line');
        line.classList.add('measure');

        // rect
        textBackground = document.createElementNS(svgNS, 'rect');
        textBackground.setAttribute('height', '1em');
        textBackground.classList.add('measure');

        // build
        g.appendChild(textBackground);
        g.appendChild(text);
        svg.appendChild(line);
        svg.appendChild(g);

        clear();
    }

    function getTextPositionLabel() {
        var deltaX
          , deltaY
          , distance
          , svgDelta;

        deltaX = (initialPoint.x - lastPosition.x);
        deltaY = (initialPoint.y - lastPosition.y);

        svgDelta = calcSVGPosition({x: deltaX, y:deltaY});

        distance = calculate_distance(svgDelta.x, svgDelta.y);

        return parseInt(distance * 10 * pixelRatio)/10 + ' km'
    }

    function point(positionX, positionY) {

        switch(status) {
            case STATUS.NONE:
                start(positionX, positionY);
                break;

            case STATUS.STARTED:
                stop();
                break;

            case STATUS.FINISHED:
                clear();
                break;
        }
    }

    function move(positionX, positionY) {

        var linePosition
          , textContent;

        if (status !== STATUS.STARTED)
            return;

        lastPosition = { x: positionX
                       , y: positionY};

        // Line
        linePosition = calcSVGPosition({ x : positionX - DragHandler.position.x
                                       , y : positionY - DragHandler.position.y});

        line.setAttribute('x2', '' + linePosition.x);
        line.setAttribute('y2', '' + linePosition.y);

        // Text
        textContent = getTextPositionLabel();
        text.innerHTML = textContent;

        refreshSizes();
    }

    function refreshSizes() {
        var textBBox
          , textPosition;

        if (status === STATUS.NONE)
            return;

        textPosition = calcTextPosition(lastPosition);
        text.setAttribute('x', textPosition.x);
        text.setAttribute('y', textPosition.y);

        textBBox = text.getBBox();

        textBackground.setAttribute('width' , '' + textBBox.width * 1.2);
        textBackground.setAttribute('height', '' + textBBox.height * 1.2);

        textBackground.setAttribute('x', '' + parseInt(textPosition.x - textBBox.width * 0.1));
        textBackground.setAttribute('y', '' + parseInt(textPosition.y - textBBox.height * 0.9));

        svg.style.fontSize = parseInt(INITIAL_FONT_SIZE / Math.pow(ZoomHandler.zoomRatio, 2)) +'em';
    }

    function start(positionX, positionY) {
        var linePosition;

        status = STATUS.STARTED;
        svg.classList.add('measureEnable');

        linePosition = calcSVGPosition({ x: positionX - DragHandler.position.x
                                       , y: positionY - DragHandler.position.y});

        line.setAttribute('x1', '' + linePosition.x);
        line.setAttribute('y1', '' + linePosition.y);

        initialPoint = {x: positionX, y:positionY};

        move(positionX, positionY);
    }

    function stop(positionX, positionY) {
        status = STATUS.FINISHED;
        move(positionX, positionY);
    }

    function isStarted() {
        return status === STATUS.STARTED;
    }

    return { initialize   : initialize
           , isStarted    : isStarted
           , move         : move
           , point        : point
           , refreshSizes : refreshSizes};

})();

var ZoomHandler    = (function() {

    var MIN_LEVEL     = 0
      , CURRENT_LEVEL = MIN_LEVEL
      , returnObject;

    returnObject = { initialize : initialize
                   , pixelRatio : {}
                   , zoom       : zoom
                   , zoomRatio  : 1};

    function applyZoomLevel(level) {
        var minSize
          , maxSize
          , maxLevel;

        maxLevel = 100;
        minSize  = 1;
        maxSize  = 3;

        returnObject.zoomRatio = minSize + (maxSize - minSize) * (level) / maxLevel;

        document.querySelector('html').style.fontSize = returnObject.zoomRatio + 'vh';
        document.querySelector('body').dataset.zoomLevel = level;
        refreshMapSizes();
    }

    function initialize() {
        document.querySelector('body').dataset.zoomLevel = 0;
        refreshMapSizes();
    }

    function refreshMapSizes() {
        returnObject.mapHeight = mainElements.background.height;
        returnObject.mapWidth  = mainElements.background.width;

        returnObject.pixelRatio.x = mainElements.svg.viewBox.baseVal.width / returnObject.mapWidth;
        returnObject.pixelRatio.y = mainElements.svg.viewBox.baseVal.height / returnObject.mapHeight;
    }

    /**
     *
     * @param {string} direction - Direction of the zoom. Expected values : "in" or "out"
     * @param {number} positionX
     * @param {number} positionY
     */
    function zoom(direction, positionX, positionY) {

        if (direction === 'in' && CURRENT_LEVEL >= MAX_ZOOM_LEVEL)
            return;
        else if (direction === 'out' && CURRENT_LEVEL <= MIN_LEVEL)
            return;

        CURRENT_LEVEL += (direction === 'in' ? 1 : -1) * ZOOM_DELTA_LEVEL;

        if (CURRENT_LEVEL > MAX_ZOOM_LEVEL)
            CURRENT_LEVEL = MAX_ZOOM_LEVEL;

        else if (CURRENT_LEVEL < MIN_LEVEL)
            CURRENT_LEVEL = MIN_LEVEL;

        applyZoomLevel(CURRENT_LEVEL);
        DragHandler.refresh();
    }

    return returnObject;
})();